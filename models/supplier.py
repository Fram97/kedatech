# -*- coding: utf-8 -*-

from odoo import models, fields # Mandatory


class Supplier(models.Model):
    _name = 'kedatech.supplier' # name_of_module.name_of_class 
    _description = 'Supplier' # Some note of table

    # Header
    name = fields.Char()