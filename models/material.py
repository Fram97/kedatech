# -*- coding: utf-8 -*-

from odoo import models, fields, api
from odoo.exceptions import ValidationError


class Material(models.Model):
    _name = 'kedatech.material' # name_of_module.name_of_class 
    _description = 'Material' # Some note of table

    # Header
    name = fields.Char(string="Material Name")
    material_code = fields.Char()
    material_buy_price = fields.Float()
    material_type = fields.Selection([('Fabric', 'Fabric'), ('Jeans', 'Jeans'), ('Cotton', 'Cotton')])
    supplier_id = fields.Many2one(comodel_name='kedatech.supplier')

    @api.constrains('material_buy_price')
    def _check_material_buy_price(self):
        for record in self:
            if record.material_buy_price < 100:
                raise ValidationError("Material Price tidak boleh kurang dari 100")