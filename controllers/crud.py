# HTTP controller of REST resources:
from odoo import http
from odoo.http import request
from random import randint
import werkzeug.wrappers
import json
import traceback
from odoo.exceptions import ValidationError
import logging

# Get the logger object
_logger = logging.getLogger(__name__)

def serialize(data):

    def serialize_dict(values):
        from datetime import date
        from datetime import datetime

        if isinstance(values, dict):
            for key in values.keys():
                # if key == 'doc' or key == 'photo' or key == 'image' or key == 'photo1' or key == 'photo2':
                #     values[key] = base64.b64encode(values.get(key))
                if isinstance(values.get(key), datetime):
                    values[key] = values.get(key).isoformat()
                elif isinstance(values.get(key), date):
                    values[key] = values.get(key).isoformat()
                elif isinstance(values.get(key), list):
                    for data in values.get(key):
                        serialize(data)
                elif isinstance(values.get(key), dict):
                    serialize_dict(values.get(key))
                # because read_group didn't open params 'load' there will be error on m2x field because the result will be lazy loaded
                # see https://github.com/odoo/odoo/issues/34432
                # the return will be tuple something like this 'partner_id': (3, <odoo.tools.func.lazy object at 0x107ceb3f0>)
                # to work around this we will transform to dict of {id : data[0], name: data[1]_rec_name}
                # we include rec_name so the caller not need to.
                # in future probably need to become params what field to be included
                # TODO detect normal tuple and lazy loaded query result
                # TODO Key of dict to _rec_name value
                elif isinstance(values.get(key), tuple):
                    values[key] = {
                        'id': values.get(key)[0],
                        'name': str(values.get(key)[1])  # TODO fix this
                    }
                    serialize_dict(values.get(key))

    if isinstance(data, list):
        for d in data:
            serialize_dict(d)
    elif isinstance(data, dict):
        serialize_dict(data)
    return data


class MaterialController(http.Controller):

    @http.route('/kedatech/material', auth='public', methods=['GET'], csrf=False)
    def get_materials(self, **kwargs):
        # Execute your SQL query and retrieve the data
        query = """
            SELECT * from kedatech_material;
        """
        request.cr.execute(query)
        results = request.cr.dictfetchall()

        # Format the results as needed (e.g., to JSON)
        formatted_data = [{'name': r['name'],
                            'material_code': r['material_code'],
                            'material_buy_price': r['material_buy_price'],
                            'material_type': r['material_type'],
                            'supplier_id': r['supplier_id'],
                            } for r in results]

        # Return the formatted data as a JSON response
        return http.Response(json.dumps(formatted_data), content_type='application/json')



    @http.route('/api/materials', auth='public', methods=['POST'], type='json')
    def create_material(self, **kw):
        try:
            # Create a new material record
            material_data = {
                'name': kw.get('material_name'),
                'material_code': kw.get('material_code'),
                'material_buy_price': kw.get('material_buy_price'),
                'material_type': kw.get('material_type'),
                'supplier_id': int(kw.get('supplier_id'))
            }
            print(material_data.name, "ininamanya")
            material = request.env['kedatech.material'].sudo().create(material_data)

            # Return a success response with the newly created material's ID
            response_data = {
                'success': True,
                'message': 'Material created successfully',
                'material_id': material.id
            }

            return json.dumps(response_data)

        except ValidationError as e:
            # Handle validation errors
            response_data = {
                'success': False,
                'message': str(e)
            }

            return json.dumps(response_data)

        except Exception as e:
            # Log the error message and traceback
            _logger.error(f"An error occurred while creating the material: {e}")
            _logger.error(traceback.format_exc())
            response_data = {
                'success': False,
                'message': 'An error occurred while creating the material'
            }

            return json.dumps(response_data)

    @http.route('/api/materials/<int:id>', auth='public', methods=['PUT'], type='json')
    def update_material(self, id, **kw):
        material = request.env['kedatech.material'].search([('id', '=', id)])
        if not material:
            return {'error': 'Material not found'}

        material.write({
            'material_code': kw.get('material_code'),
            'material_name': kw.get('material_name'),
            'material_type': kw.get('material_type'),
            'material_buy_price': float(kw.get('material_buy_price')),
            'supplier_id': int(kw.get('supplier_id'))
        })

        return material.read()

    @http.route('/api/materials/<int:id>', auth='public', methods=['DELETE'], type='json')
    def delete_material(self, id, **kw):
        material = request.env['kedatech.material'].search([('id', '=', id)])
        if not material:
            return {'error': 'Material not found'}
        
        material.unlink()
        return {'result': 'Material deleted successfully'}
