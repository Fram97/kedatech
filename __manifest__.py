# -*- coding: utf-8 -*-
{
    'name': "Kedatech",

    'summary': """
        Module Kedatech tes""",

    'description': """
        Kedatech
    """,

    'author': "Febry Ramadhan",
    'website': "framad.github.io/framadhan",

    'category': 'Uncategorized',
    'version': '0.1',

		# Depencicy
    'depends': ['base'],

		# Include ALL XML Code in Here be mindful of order
    'data': [
        'security/ir.model.access.csv',
        'views/menuitems.views.xml',
        'views/material/material.action.views.xml',
        'views/supplier/supplier.action.views.xml'
    ],

    'installable': True,
    'application': True,
    'auto_install': False

}